package main

import (
	"github.com/elastic/go-elasticsearch" // Импорт пакета для работы с Elasticsearch
	"github.com/sirupsen/logrus"          // Импорт пакета для логирования
	"gitlab.com/dnpp_public/elogrus"      // Импорт пакета для интеграции Elasticsearch с Logrus
)

func main() {
	log, err := NewLog() // Создание нового логгера
	if err != nil {
		panic(err) // Прекратить выполнение программы, если есть ошибка
	}

	log.Info("Запись в лог") // Запись информационного сообщения в лог
	log.Fatal("Ошибка")      // Запись фатального сообщения в лог и завершение программы
}

func NewLog() (*logrus.Logger, error) {
	log := logrus.New()     // Создание нового экземпляра логгера
	log.ReportCaller = true // Включение отчета о вызывающем коде

	debugLevel := logrus.InfoLevel // Установка уровня логирования
	log.SetLevel(debugLevel)

	// Создание нового клиента Elasticsearch с указанным адресом
	client, err := elasticsearch.NewClient(elasticsearch.Config{
		Addresses: []string{"http://10.15.59.5:9200"},
	})
	if err != nil {
		// Если подключение к Elasticsearch не удалось, вывод предупреждения
		log.Warnf("Could not connect to Elasticsearch: %v. Logrus will log to console instead.", err)
	} else {
		// Создание хука для отправки логов в Elasticsearch
		hook, err := elogrus.NewElasticHook(client, "10.15.59.3", "Zakupki-PP", "golang", debugLevel, "etl_logs")
		if err != nil {
			// Если создание хука не удалось, вывод предупреждения
			log.Warnf("Could not create ElasticHook: %v. Logrus will log to console instead.", err)
		} else {
			log.Hooks.Add(hook) // Добавление хука к логгеру
		}
	}

	return log, nil // Возврат логгера
}
