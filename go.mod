module es-logs-example

go 1.20

require (
	github.com/elastic/go-elasticsearch v0.0.0
	github.com/sirupsen/logrus v1.9.3
	gitlab.com/dnpp_public/elogrus v1.0.2
)

require (
	github.com/elastic/go-elasticsearch/v8 v8.2.0 // indirect
	golang.org/x/sys v0.0.0-20220715151400-c0bba94af5f8 // indirect
)
